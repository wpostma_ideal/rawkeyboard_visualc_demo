/* raw keyboard sample from raymond chen's blog 

 
*/

#pragma comment(lib, "comctl32.lib")

#include <windows.h>
#include <windowsx.h>
#include <ole2.h>
#include <commctrl.h>
#include <shlwapi.h>
#include <strsafe.h> // StringCchCat?

//#include "stdafx.h"

//#define STRICT

HINSTANCE g_hinst;                          /* This application's HINSTANCE */
HWND g_hwndChild;                           /* Optional child window */


int g_shifted;
/*
 *  OnSize
 *      If we have an inner child, resize it to fit.
 */
void
OnSize(HWND hwnd, UINT state, int cx, int cy)
{
	if (g_hwndChild) {
		MoveWindow(g_hwndChild, 0, 0, cx, cy, TRUE);
	}
}


/*
 *  OnCreate
 *      Applications will typically override this and maybe even
 *      create a child window.
 */

BOOL
OnCreate(HWND hwnd, LPCREATESTRUCT lpcs)
{
	g_shifted = 0; // SHIFT STATE

	g_hwndChild = CreateWindow(TEXT("listbox"), NULL,
		LBS_HASSTRINGS | WS_CHILD | WS_VISIBLE | WS_VSCROLL,
		0, 0, 0, 0, hwnd, NULL, g_hinst, 0);

	RAWINPUTDEVICE dev;
	dev.usUsagePage = 1;
	dev.usUsage = 6;
	dev.dwFlags = 0;
	dev.hwndTarget = hwnd;
	RegisterRawInputDevices(&dev, 1, sizeof(dev));

	return TRUE;
}


/*
 *  OnDestroy
 *      Post a quit message because our application is over when the
 *      user closes this window.
 */


void
OnDestroy(HWND hwnd)
{
	RAWINPUTDEVICE dev;
	dev.usUsagePage = 1;
	dev.usUsage = 6;
	dev.dwFlags = RIDEV_REMOVE;
	dev.hwndTarget = hwnd;
	RegisterRawInputDevices(&dev, 1, sizeof(dev));

	PostQuitMessage(0);
}


void KeyToString(TCHAR *ascii_value, int shiftstate, int virtualKey, int scanCode)
{
		//UINT scanCode = MapVirtualKey(scancode, MAPVK_VK_TO_VSC);
	int result = 0;


		if (shiftstate==1)
		{
			switch (virtualKey)
			{
			 case 0x31:
				 StringCbCopy(ascii_value, 128, TEXT("!") );
				 return;
			 case 0x32:
				 StringCbCopy(ascii_value, 128, TEXT("@"));
				 return;
			 case 0x33:
				 StringCbCopy(ascii_value, 128, TEXT("#"));
				 return;
			 case 0x34:
				 StringCbCopy(ascii_value, 128, TEXT("$"));
				 return;
			 case 0x35:
				 StringCbCopy(ascii_value, 128, TEXT("%"));
				 return;
			 case 0x36:
				 StringCbCopy(ascii_value, 128, TEXT("^"));
				 return;
			 case 0x37:
				 StringCbCopy(ascii_value, 128, TEXT("&"));
				 return;
			 case 0x38:
				 StringCbCopy(ascii_value, 128, TEXT("*"));
				 return;

					
			}

		}
		
			switch (virtualKey)
			{
			case VK_LEFT: case VK_UP: case VK_RIGHT: case VK_DOWN:
			case VK_RCONTROL: case VK_RMENU:
			case VK_LWIN: case VK_RWIN: case VK_APPS:
			case VK_PRIOR: case VK_NEXT:
			case VK_END: case VK_HOME:
			case VK_INSERT: case VK_DELETE:
			case VK_DIVIDE:
			case VK_NUMLOCK:
				scanCode |= KF_EXTENDED;
			default:

				result = GetKeyNameText(scanCode << 16, ascii_value, 128);
			}
		


}

/* raw keyboard input */

#define HANDLE_WM_INPUT(hwnd, wParam, lParam, fn) \
  ((fn)((hwnd), GET_RAWINPUT_CODE_WPARAM(wParam), \
        (HRAWINPUT)(lParam)), 0)

void OnInput(HWND hwnd, WPARAM code, HRAWINPUT hRawInput)
{
	UINT dwSize;
	GetRawInputData(hRawInput, RID_INPUT, nullptr,
		&dwSize, sizeof(RAWINPUTHEADER));
	RAWINPUT *input = (RAWINPUT *)malloc(dwSize);
	GetRawInputData(hRawInput, RID_INPUT, input,
		&dwSize, sizeof(RAWINPUTHEADER));

if (input->header.dwType == RIM_TYPEKEYBOARD) {
		TCHAR prefix[80];
		prefix[0] = TEXT('\0');
		if (input->data.keyboard.Flags & RI_KEY_E0) {
			StringCchCat(prefix, ARRAYSIZE(prefix), TEXT("E0 "));
		}
		if (input->data.keyboard.Flags & RI_KEY_E1) {
			StringCchCat(prefix, ARRAYSIZE(prefix), TEXT("E1 "));
		}

		TCHAR ascii_value[256];
		memset(ascii_value, 0, 256);

		ascii_value[0]  = '\0';

		// this appears to duplicate the input->data.Keyboard.MakeCode:
		// int ScanCode = MapVirtualKey(input->data.keyboard.VKey, MAPVK_VK_TO_VSC);
		int ScanCode  = input->data.keyboard.MakeCode;
		int VirtualKey = input->data.keyboard.VKey;


		int l_pressed = !(input->data.keyboard.Flags & RI_KEY_BREAK);

		if (ScanCode == 42) /* shift key code, and it's the key DOWN */
		{
			g_shifted = l_pressed;
		}
		
		// using shift state
		KeyToString(ascii_value, g_shifted, VirtualKey, ScanCode);



		TCHAR buffer[256];
		



		StringCchPrintf(buffer, ARRAYSIZE(buffer),
			TEXT("%p, msg=%04x, vk=%04x, scanCode=%s%02x, %s   ::: flags=%04x %s"),
			input->header.hDevice,
			input->data.keyboard.Message,
			input->data.keyboard.VKey,
			prefix,
			input->data.keyboard.MakeCode,
			l_pressed  ? /*TRUE=*/ TEXT("press") : TEXT("release"),
			input->data.keyboard.Flags,
			ascii_value
			);
		ListBox_AddString(g_hwndChild, buffer);
	}
	DefRawInputProc(&input, 1, sizeof(RAWINPUTHEADER));
	free(input);
}

/*
 *  PaintContent
 *      Interesting things will be painted here eventually.
 */
void
PaintContent(HWND hwnd, PAINTSTRUCT *pps)
{
}
/*
 *  OnPaint
 *      Paint the content as part of the paint cycle.
 */
void
OnPaint(HWND hwnd)
{
	PAINTSTRUCT ps;
	BeginPaint(hwnd, &ps);
	PaintContent(hwnd, &ps);
	EndPaint(hwnd, &ps);
}
/*
 *  OnPrintClient
 *      Paint the content as requested by USER.
 */
void
OnPrintClient(HWND hwnd, HDC hdc)
{
	PAINTSTRUCT ps;
	ps.hdc = hdc;
	GetClientRect(hwnd, &ps.rcPaint);
	PaintContent(hwnd, &ps);
}

/*
 *  Window Message Handling
 */

LRESULT CALLBACK
WndProc(HWND hwnd, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uiMsg) {
		HANDLE_MSG(hwnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hwnd, WM_SIZE, OnSize);
		HANDLE_MSG(hwnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hwnd, WM_PAINT, OnPaint);

		HANDLE_MSG(hwnd, WM_INPUT, OnInput);  /*  ->HANDLE_WM_INPUT */

	case WM_PRINTCLIENT: OnPrintClient(hwnd, (HDC)wParam); return 0;
	}
	return DefWindowProc(hwnd, uiMsg, wParam, lParam);
}
BOOL
InitApp(void)
{
	WNDCLASS wc;
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = g_hinst;
	wc.hIcon = NULL;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = TEXT("RawKeyboardDemo");
	if (!RegisterClass(&wc)) return FALSE;
	InitCommonControls();               /* In case we use a common control */
	return TRUE;
}
int WINAPI WinMain(HINSTANCE hinst, HINSTANCE hinstPrev,
	LPSTR lpCmdLine, int nShowCmd)
{
	MSG msg;
	HWND hwnd;
	g_hinst = hinst;
	if (!InitApp()) return 0;
	if (SUCCEEDED(CoInitialize(NULL))) {/* In case we use COM */
		hwnd = CreateWindow(
			TEXT("RawKeyboardDemo"),                /* Class Name */
			TEXT("Raw Keyboard Demo"),                /* Title CAPTION */
			WS_OVERLAPPEDWINDOW,            /* Style */
			CW_USEDEFAULT, CW_USEDEFAULT,   /* Position */
			CW_USEDEFAULT, CW_USEDEFAULT,   /* Size */
			NULL,                           /* Parent */
			NULL,                           /* No menu */
			hinst,                          /* Instance */
			0);                             /* No special parameters */
		ShowWindow(hwnd, nShowCmd);
		while (GetMessage(&msg, NULL, 0, 0)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		CoUninitialize();
	}
	return 0;
}